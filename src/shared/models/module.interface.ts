import { Router } from "express";

export interface Module {
  path: string;
  router: Router;
}
