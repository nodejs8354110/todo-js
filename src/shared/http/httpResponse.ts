import { Response } from "express";

export class HttpResponse {
  static ok<T>(res: Response, data: T | null = null) {
    res.status(200).json({
      statusCode: 200,
      descriptionCode: "OK",
      data,
    });
  }

  static created<T>(res: Response, data: T | null = null) {
    res.status(201).json({
      statusCode: 201,
      descriptionCode: "CREATED",
      data,
    });
  }

  static badRequest<T>(res: Response, errors: T | null = null) {
    res.status(400).json({
      statusCode: 400,
      descriptionCode: "BAD_REQUEST",
      errors,
    });
  }

  static unauthorized(res: Response) {
    res.status(401).json({
      statusCode: 401,
      descriptionCode: "UNAUTHORIZED",
    });
  }

  static notFound(res: Response, error: string | null = null) {
    res.status(404).json({
      statusCode: 404,
      descriptionCode: "NOT_FOUND",
      error,
    });
  }

  static notAcceptable(res: Response, error: string | null = null) {
    return res.status(406).json({
      statusCode: 406,
      descriptionCode: "NOT_ACCEPTABLE",
      error,
    });
  }

  static internalError(res: Response) {
    return res.status(500).json({
      statusCode: 500,
      descriptionCode: "INTERNAL_ERROR",
    });
  }
}
