import jsonwebtoken from "jsonwebtoken";
import config from "../../config";

export class JwtService {
  static sign(
    id: string,
    email: string,
    username: string,
    createdAt: Date,
    updatedAt: Date,
  ) {
    return jsonwebtoken.sign(
      {
        id,
        email,
        username,
        updatedAt,
        createdAt,
      },
      config.jwtSecret,
      {
        expiresIn: "2h",
      },
    );
  }
}
