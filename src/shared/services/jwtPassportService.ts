import passportLib, { PassportStatic } from "passport";
import { JwtFromRequestFunction, Strategy as JwtStrategy } from "passport-jwt";
import { ExtractJwt } from "passport-jwt";
import config from "../../config";
import prisma from "../../database/database";
import { NextFunction, Request, Response } from "express";
import { HttpResponse } from "../http/httpResponse";

export class JwtPassportService {
  private jwtOptions: {
    jwtFromRequest: JwtFromRequestFunction;
    secretOrKey: string;
  };
  passport: PassportStatic;

  constructor() {
    this.jwtOptions = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.jwtSecret,
    };
    this.passport = passportLib;
    this.passport.use(
      new JwtStrategy(this.jwtOptions, this.verifyUser.bind(this)),
    );
    this.authenticate = this.authenticate.bind(this);
  }

  async verifyUser(jwtPayload: any, done: any) {
    try {
      const user = await prisma.user.findUnique({
        where: {
          id: jwtPayload.id,
        },
      });
      if (!user) return done(null, false);
      return done(null, {
        id: user.id,
        email: user.email,
        username: user.username,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      });
    } catch (err) {
      return done(err, false);
    }
  }

  authenticate(req: Request, res: Response, next: NextFunction) {
    return this.passport.authenticate("jwt", (err: any, user: any) => {
      if (err || !user) return HttpResponse.unauthorized(res);
      req.user = user;
      next();
    })(req, res, next);
  }
}

const jwtPassportService = new JwtPassportService();

export default jwtPassportService;
