import bcrypt from "bcrypt";

export class BcryptService {
  static async hash(data: string) {
    const salt = await bcrypt.genSalt();
    return bcrypt.hash(data, salt);
  }

  static compare(data: string, dataEncrypt: string) {
    return bcrypt.compare(data, dataEncrypt);
  }
}
