import * as Yup from "yup";

export class BaseValidation {
  setSchema(baseObject: Yup.AnyObject) {
    return Yup.object(baseObject);
  }
}
