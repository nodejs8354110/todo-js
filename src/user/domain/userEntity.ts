export class UserEntity {
  id: string;
  email: string;
  username: string;
  createdAt: string;
  updatedAt: string;

  constructor(
    id: string,
    email: string,
    username: string,
    createdAt: string,
    updatedAt: string,
  ) {
    this.id = id;
    this.email = email;
    this.username = username;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
