import prisma from "../../database/database";

export class UserRepository {
  static insertOne(email: string, username: string, password: string) {
    return prisma.user.create({
      data: {
        email,
        username,
        password,
      },
    });
  }

  static findOne(email: string) {
    return prisma.user.findUnique({
      where: {
        email,
      },
    });
  }
}
