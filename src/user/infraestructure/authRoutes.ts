import { Router } from "express";
import { Module } from "../../shared/models/module.interface";
import { AuthController } from "../application/controllers/authController";
import { UserMiddleware } from "../application/middlewares/userMiddlewares";

export class AuthRoutes implements Module {
  path: string;
  router: Router;
  controller: AuthController;

  constructor() {
    this.path = "/v1/auth";
    this.router = Router();
    this.controller = new AuthController();
    this.initRoutes();
  }

  initRoutes() {
    this.router.post("/sign-up", UserMiddleware.signUp, this.controller.signUp);
    this.router.post("/sign-in", UserMiddleware.signIn, this.controller.signIn);
  }
}
