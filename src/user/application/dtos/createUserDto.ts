import { IsEmail, IsNotEmpty, IsString, Length, validate } from "class-validator";

class CreateUserValidate {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Length(4, 50)
  username: string;

  @IsString()
  @IsNotEmpty()
  @Length(8, 16)
  password: string;
}

export class CreateUserDTO {
  createUserValidate: CreateUserValidate;

  constructor(email: string, username: string, password: string) {
    this.createUserValidate = new CreateUserValidate();
    this.createUserValidate.email = email;
    this.createUserValidate.username = username;
    this.createUserValidate.password = password;
  }

  async validation() {
    const errors = await validate(this.createUserValidate);
    if(errors.length) throw { errors: errors.map(err => err.constraints) }
  }
}
