import { IsEmail, IsNotEmpty, IsString, Length, validate } from "class-validator";

export class LoginValidate {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Length(8, 16)
  password: string;
}


export class LoginDTO {
  loginValidate: LoginValidate;

  constructor(email: string, password: string) {
    this.loginValidate = new LoginValidate();
    this.loginValidate.email = email;
    this.loginValidate.password = password;
  }

  async validation() {
    const errors = await validate(this. loginValidate);
    if(errors.length) throw { errors: errors.map(err => err.constraints) }
  }
}
