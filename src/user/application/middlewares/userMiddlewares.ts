import { NextFunction, Request, Response } from "express";
import { HttpResponse } from "../../../shared/http/httpResponse";
import { CreateUserDTO } from "../dtos/createUserDto";
import { LoginDTO } from "../dtos/loginDto";

export class UserMiddleware {
  static signUp(req: Request, res: Response, next: NextFunction) {
    const { email, username, password } = req.body || { undefined };
    const userSchema = new CreateUserDTO(email, username, password);
    userSchema
      .validation()
      .then(() => next())
      .catch(({ errors }) =>
        HttpResponse.badRequest(res, errors),
      );
  }

  static signIn(req: Request, res: Response, next: NextFunction) {
    const { email, password } = req.body || {
      undefined,
    };
    const userSchema = new LoginDTO(email, password);
    userSchema
      .validation()
      .then(() => next())
      .catch(({ errors }) =>
        HttpResponse.badRequest(res, errors),
      );
  }
}
