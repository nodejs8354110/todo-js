import { UserRepository } from "../../infraestructure/userRepository";

export class UserService {
  create(email: string, username: string, password: string) {
    return UserRepository.insertOne(email, username, password);
  }

  findOne(email: string) {
    return UserRepository.findOne(email);
  }
}
