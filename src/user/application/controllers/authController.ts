import { Request, Response } from "express";
import { HttpResponse } from "../../../shared/http/httpResponse";
import { BcryptService } from "../../../shared/services/bcryptService";
import { JwtService } from "../../../shared/services/jwtService";
import { UserService } from "../services/userService";

export class AuthController {
  constructor(private userService = new UserService()) {
    this.userService = userService;
    this.signIn = this.signIn.bind(this);
    this.signUp = this.signUp.bind(this);
  }

  async signIn(req: Request, res: Response): Promise<void> {
    const { email, password } = req.body;
    try {
      const user = await this.userService.findOne(email);
      if (!user || !(await BcryptService.compare(password, user.password))) {
        HttpResponse.notAcceptable(res, "Email or password incorrect");
        return;
      }
      const token = JwtService.sign(
        user.id,
        email,
        user.username,
        user.createdAt,
        user.updatedAt,
      );
      HttpResponse.ok(res, { token });
    } catch {
      HttpResponse.internalError(res);
    }
  }

  async signUp(req: Request, res: Response): Promise<void> {
    const { email, username, password } = req.body;
    try {
      const hash = await BcryptService.hash(password);
      await this.userService.create(email, username, hash);
      HttpResponse.created(res);
    } catch (err: any) {
      if (err?.code === "P2002") {
        HttpResponse.notAcceptable(
          res,
          `Field duplicate: ${err.meta.target[0].toUpperCase()}`,
        );
        return;
      }
      HttpResponse.internalError(res);
    }
  }
}
