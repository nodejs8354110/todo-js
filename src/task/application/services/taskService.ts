import { TaskRepository } from "../../infraestructure/taskRepository";

export class TaskService {
  async getPagination(userId: string, records: number, page: number) {
    const tasks = await TaskRepository.getAll(userId, records, page);
    const totalTasks = await TaskRepository.getCount(userId);
    return { tasks, totalTasks };
  }

  create(description: string, completed: boolean, userId: string) {
    return TaskRepository.insertOne(description, completed, userId);
  }

  updateOne(
    userId: string,
    id: string,
    description: string,
    completed: boolean,
  ) {
    return TaskRepository.updateOne(userId, id, description, completed);
  }

  deleteOne(userId: string, id: string) {
    return TaskRepository.deleteOne(userId, id);
  }
}
