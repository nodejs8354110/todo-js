import { NextFunction, Request, Response } from "express";
import { HttpResponse } from "../../../shared/http/httpResponse";
import { CreateTaskDTO } from "../dtos/createTaskDto";
import { DeleteTaskDTO } from "../dtos/deleteTaskDto";
import { PaginationTaskDTO } from "../dtos/paginationTaskDto";
import { UpdateTaskDTO } from "../dtos/updateTaskDto";

export class TaskMiddleware {
  static paginationValidate(req: Request, res: Response, next: NextFunction) {
    const { page = "0", records = "20" } = req.query;
    const taskSchema = new PaginationTaskDTO(Number(records), Number(page));
    taskSchema
      .validation()
      .then(() => next())
      .catch(({ errors }) => HttpResponse.badRequest(res, errors));
  }

  static createValidate(req: Request, res: Response, next: NextFunction) {
    const { description, completed } = req.body || { undefined };
    const taskSchema = new CreateTaskDTO(description, completed);
    taskSchema
      .validation()
      .then(() => next())
      .catch(({ errors }) => HttpResponse.badRequest(res, errors));
  }

  static updateValidate(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const { description, completed } = req.body || { undefined };
    const taskSchema = new UpdateTaskDTO(id, description, completed);
    taskSchema
      .validation()
      .then(() => next())
      .catch(({ errors }) => HttpResponse.badRequest(res, errors));
  }

  static deleteValidate(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const taskSchema = new DeleteTaskDTO(id);
    taskSchema
      .validation()
      .then(() => next())
      .catch(({ errors }) => HttpResponse.badRequest(res, errors));
  }
}
