import { User } from "@prisma/client";
import { Request, Response } from "express";
import { HttpResponse } from "../../../shared/http/httpResponse";
import { TaskEntity } from "../../domain/taskEntity";
import { TaskService } from "../services/taskService";

export class TaskController {
  constructor(private taskService = new TaskService()) {
    this.taskService = taskService;
    this.getTasks = this.getTasks.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
  }

  getTasks(req: Request, res: Response) {
    const { id } = req.user as User;
    const { page = "0", records = "20" } = req.query;
    this.taskService
      .getPagination(id, Number(records), Number(page))
      .then(({ tasks, totalTasks }) =>
        HttpResponse.ok(res, {
          tasks: tasks.map(
            task => new TaskEntity(task.id, task.description, task.completed),
          ),
          totalTasks,
        }),
      )
      .catch(() => HttpResponse.internalError(res));
  }

  create(req: Request, res: Response) {
    const { description, completed } = req.body;
    const { id } = req.user as User;
    this.taskService
      .create(description, completed, id)
      .then(todo =>
        HttpResponse.created(
          res,
          new TaskEntity(todo.id, todo.description, todo.completed),
        ),
      )
      .catch(() => HttpResponse.internalError(res));
  }

  update(req: Request, res: Response) {
    const { id } = req.params;
    const { description, completed } = req.body;
    const { id: idUser } = req.user as User;
    this.taskService
      .updateOne(idUser, id, description, completed)
      .then(todo =>
        HttpResponse.ok(
          res,
          new TaskEntity(todo.id, todo.description, todo.completed),
        ),
      )
      .catch(err => {
        if (err?.code === "P2025")
          return HttpResponse.notFound(res, "The task could not be found");
        HttpResponse.internalError(res);
      });
  }

  deleteTask(req: Request, res: Response) {
    const { id } = req.params;
    const { id: idUser } = req.user as User;
    this.taskService
      .deleteOne(idUser, id)
      .then(() => HttpResponse.ok(res))
      .catch(err => {
        if (err?.code === "P2025")
          return HttpResponse.notFound(res, "The task could not be found");
        HttpResponse.internalError(res);
      });
  }
}
