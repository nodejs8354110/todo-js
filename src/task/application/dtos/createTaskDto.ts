import { IsBoolean, IsNotEmpty, IsOptional, IsString, Length, validate } from "class-validator";

export class CreateTaskValidation {
  @IsString()
  @IsNotEmpty()
  @Length(4, 80)
  description: string;

  @IsBoolean()
  @IsOptional()
  completed?: boolean;
}


export class CreateTaskDTO {
  private createTaskValidation: CreateTaskValidation;

  constructor(
    description: string,
    completed?: boolean
  ) {
    this.createTaskValidation = new CreateTaskValidation();
    this.createTaskValidation.description = description;
    this.createTaskValidation.completed = completed;
  }

  async validation() {
    const errors = await validate(this.createTaskValidation);
    if(errors.length) throw { errors: errors.map(err => err.constraints) }
  }
}