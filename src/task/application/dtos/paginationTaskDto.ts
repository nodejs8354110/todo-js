import { IsNumber, IsOptional, Max, Min, validate } from "class-validator";

export class PaginationTaskValidation {
  @IsNumber()
  @IsOptional()
  @Min(0)
  @Max(20)
  records?: number;

  @IsNumber()
  @IsOptional()
  @Min(0)
  @Max(100)
  page?: number;
}

export class PaginationTaskDTO {
  private paginationTaskValidation: PaginationTaskValidation;

  constructor(records?: number, page?: number) {
    this.paginationTaskValidation = new PaginationTaskValidation();
    this.paginationTaskValidation.records = records;
    this.paginationTaskValidation.page = page;
  }

  async validation() {
    const errors = await validate(this.paginationTaskValidation);
    if (errors.length) throw { errors: errors.map(err => err.constraints) };
  }
}
