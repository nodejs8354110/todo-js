import { IsNotEmpty, IsString, IsUUID, validate } from "class-validator";

export class DeleteTaskValidation {
  @IsString()
  @IsUUID()
  @IsNotEmpty()
  id: string;
}

export class DeleteTaskDTO {
  private deleteTaskValidation: DeleteTaskValidation;

  constructor(id: string) {
    this.deleteTaskValidation.id = id;
  }

  async validation() {
    const errors = await validate(this.deleteTaskValidation);
    if(errors.length) throw { errors: errors.map(err => err.constraints) }
  }
}
