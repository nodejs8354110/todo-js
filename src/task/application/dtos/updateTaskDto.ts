import { IsBoolean, IsNotEmpty, IsOptional, IsString, IsUUID, Length, validate } from "class-validator";

export class UpdateTaskValidation  {
  @IsString()
  @IsUUID()
  @IsNotEmpty()
  id: string;

  @IsString()
  @IsNotEmpty()
  @Length(4, 80)
  description: string;

  @IsBoolean()
  @IsOptional()
  completed?: boolean;
}

export class UpdateTaskDTO  {
  private updateTaskValidation: UpdateTaskValidation;

  constructor(id: string, description: string, completed?: boolean) {
    this.updateTaskValidation = new UpdateTaskValidation();
    this.updateTaskValidation.id = id;
    this.updateTaskValidation.description = description;
    this.updateTaskValidation.completed = completed;
  }

  async validation() {
    const errors = await validate(this.updateTaskValidation);
    if(errors.length) throw { errors: errors.map(err => err.constraints) }
  }
}
