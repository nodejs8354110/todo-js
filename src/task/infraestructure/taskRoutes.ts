import { Router } from "express";
import { Module } from "../../shared/models/module.interface";
import jwtPassportService from "../../shared/services/jwtPassportService";
import { TaskController } from "../application/controllers/taskController";
import { TaskMiddleware } from "../application/middlewares/taskMiddleware";

export class TaskRoutes implements Module {
  path: string;
  router: Router;
  controller: TaskController;

  constructor() {
    this.path = "/v1/todo";
    this.router = Router();
    this.controller = new TaskController();
    this.initRoutes();
  }

  initRoutes() {
    this.router.get(
      "/",
      jwtPassportService.authenticate,
      TaskMiddleware.paginationValidate,
      this.controller.getTasks,
    );
    this.router.post(
      "/",
      jwtPassportService.authenticate,
      TaskMiddleware.createValidate,
      this.controller.create,
    );
    this.router.put(
      "/:id",
      jwtPassportService.authenticate,
      TaskMiddleware.updateValidate,
      this.controller.update,
    );
    this.router.delete(
      "/:id",
      jwtPassportService.authenticate,
      TaskMiddleware.deleteValidate,
      this.controller.deleteTask,
    );
  }
}
