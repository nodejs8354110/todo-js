import prisma from "../../database/database";

export class TaskRepository {
  static getAll(userId: string, records: number, page: number) {
    return prisma.task.findMany({
      skip: page * records,
      take: records,
      where: { userId },
    });
  }

  static getCount(userId: string) {
    return prisma.task.count({
      where: { userId },
    });
  }

  static insertOne(description: string, completed: boolean, userId: string) {
    return prisma.task.create({
      data: {
        description,
        completed,
        userId: userId,
      },
    });
  }

  static updateOne(
    userId: string,
    id: string,
    description: string,
    completed: boolean,
  ) {
    return prisma.task.update({
      data: { description, completed },
      where: { id, userId: userId },
    });
  }

  static deleteOne(userId: string, id: string) {
    return prisma.task.delete({
      where: { id, userId: userId },
    });
  }
}
