import dotenv from "dotenv";

class Config {
  private _host: string;
  private _port: number;
  private _jwtSecret: string;

  constructor() {
    this.setEnvironment();
    this._host = String(process.env.HOST);
    this._port = Number(process.env.PORT);
    this._jwtSecret = String(process.env.JWT_SECRET);
  }

  setEnvironment(): void {
    const environment = String(process.env.NODE_ENV);
    const path = `.env.${environment.trim()}`;
    dotenv.config({ path });
  }

  get host(): string {
    return this._host;
  }

  get port(): number {
    return this._port;
  }

  get jwtSecret(): string {
    return this._jwtSecret;
  }
}

const config = new Config();

export default config;
