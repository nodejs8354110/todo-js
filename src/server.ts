import { App } from "./app";
import { TaskRoutes } from "./task/infraestructure/taskRoutes";
import { AuthRoutes } from "./user/infraestructure/authRoutes";

const modules = [new TaskRoutes(), new AuthRoutes()];

new App(modules);
