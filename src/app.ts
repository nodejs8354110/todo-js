import express, { Express } from "express";
import morgan from "morgan";
import config from "./config";
import { Module } from "./shared/models/module.interface";
import jwtPassport from "./shared/services/jwtPassportService";

export class App {
  private app: Express;
  private basePath: string;

  constructor(modules: Module[]) {
    this.app = express();
    this.basePath = "/rest-api";
    this.initMiddlewares();
    this.initModules(modules);
    this.initServer();
  }

  private initModules(modules: Module[]): void {
    modules.map(module =>
      this.app.use(this.basePath + module.path, module.router),
    );
  }

  private initMiddlewares(): void {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(morgan("dev"));
    this.app.use(jwtPassport.passport.initialize());
  }

  private initServer(): void {
    this.app.listen(config.port, config.host, () =>
      console.log(
        "\x1b[37m",
        `🚀 Server running on: http://${config.host}:${config.port}/`,
      ),
    );
  }
}
